package ShipGame;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by damianszwichtenberg on 26.02.2016.
 */
public class Pomocnik {
    String pobieranieDanych ()  {
        String wejscie = "";
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            wejscie = br.readLine();
            if (wejscie.length() == 0)  {
                return null;
            }
        } catch (IOException e) { System.out.println("IOException " + e);}
        return wejscie;
    }
}
