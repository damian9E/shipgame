package ShipGame;

import java.util.ArrayList;

/**
 * Created by damianszwichtenberg on 26.02.2016.
 */
public class Plansza {      //poczatek klasy
    private char[] pion = {'1','2','3','4','5','6','7'};
    private char[] poziom = {'a','b','c','d','e','f','g'};
    private ArrayList<String> wybranePozycje;
    private ArrayList<String> wybranePozycjePomoc = new ArrayList<String>();
    private ArrayList<ArrayList> calosc = new ArrayList<ArrayList>();
    private int przesuniecie;
    private int liczba1;
    private int liczba2;
    private String wybor;
    void losowaniePortali()   { //poczatek metody losowaniePortalu
        while (wybranePozycjePomoc.size() <=8 ) {
            while (wybranePozycjePomoc.contains(wybor)) {
            przesuniecie = (int) (Math.random() * 4); // 0-lewo, 1-prawo, 2-gora, 3-dol
            liczba1 = (int) (Math.random() * 7);
            liczba2 = (int) (Math.random() * 7);
            wybor = Character.toString(pion[liczba1]) + Character.toString(poziom[liczba2]);
            wybranePozycje = new ArrayList<String>();
            wybranePozycje.add(wybor);
            while (wybranePozycje.size() <= 2) { //poczatek while2
                while (wybranePozycje.contains(wybor)) {  //poczatek while3
                    switch (przesuniecie) { //poczatek switcha
                        case 0: //poczatek case 0
                            liczba2--;
                            if (liczba2 < 0) { //poczatek if
                                liczba2 = liczba2 + 2;
                            } //koniec if
                            wybor = Character.toString(pion[liczba1]) + Character.toString(poziom[liczba2]);
                            break; //koniec case 0
                        case 1: //poczatek case 2
                            liczba2++;
                            if (liczba2 > 6) { //poczatek if
                                liczba2 = liczba2 - 2;
                            } //koniec if
                            wybor = Character.toString(pion[liczba1]) + Character.toString(poziom[liczba2]);
                            break; //koniec case 2
                        case 2: //poczatek case 1
                            liczba1--;
                            if (liczba1 < 0) { //poczatek if
                                liczba1 = liczba1 + 2;
                            } //koniec if
                            wybor = Character.toString(pion[liczba1]) + Character.toString(poziom[liczba2]);
                            break; //koniec case 1
                        case 3: //poczatek case 3
                            liczba1++;
                            if (liczba1 > 6) { //poczatek if
                                liczba1 = liczba1 - 2;
                            } //koniec if
                            wybor = Character.toString(pion[liczba1]) + Character.toString(poziom[liczba2]);
                            break; //koniec case 3
                    } //koniec switcha
                } //koniec while3
                if (!wybranePozycje.contains(wybor)) {
                    wybranePozycje.add(wybor);
                    wybranePozycjePomoc.add(wybor);
                }
                if (przesuniecie == 0 || przesuniecie == 1) {
                    przesuniecie = (int) (Math.random() * 2);
                } else przesuniecie = (int) ((Math.random() + 1) * 2);
            } //koniec while2
            calosc.add(wybranePozycje);
        }
        }

    }  //koniec metody losowaniePortalu

    void towrzeniePortali() {
        Portal portal1 = new Portal();
        Portal portal2 = new Portal();
        Portal portal3 = new Portal();
        portal1.setNazwa("www.onet.pl");
        portal2.setNazwa("www.o2.pl");
        portal3.setNazwa("Go.com");
        portal1.setPolozenie(calosc.get(0));
        portal2.setPolozenie(calosc.get(1));
        portal2.setPolozenie(calosc.get(2));
    }

    ArrayList zwrocDane()    {
        return calosc;
    }

    void wypisaniePozycji ()    {
        System.out.println("pozycja: " + wybranePozycje.get(0));
        System.out.println("pozycja: " + wybranePozycje.get(1));
        System.out.println("pozycja: " + wybranePozycje.get(2));
        System.out.println("calosc " + calosc);
    }
} //koniec klasy
