package ShipGame;

import java.util.ArrayList;

/**
 * Created by damianszwichtenberg on 02.03.2016.
 */
public class Plansza2 {

    private char[] pion = {'1','2','3','4','5','6','7'};
    private char[] poziom = {'a','b','c','d','e','f','g'};
    private ArrayList<String> wybranePozycje = new ArrayList<String>();
    private ArrayList<String> wybranePozycjePomoc = new ArrayList<String>();
    private ArrayList<ArrayList> calosc = new ArrayList<ArrayList>();
    private int przesuniecie;
    private int liczba1;
    private int liczba2;
    private String wybor;

    void losowaniePortali() {
        while (wybranePozycjePomoc.size() <= 8) {
            liczba1 = (int) (Math.random() * 7);
            liczba2 = (int) (Math.random() * 7);
            przesuniecie = (int) (Math.random() * 4);
            wybor = Character.toString(pion[liczba1]) + Character.toString(poziom[liczba2]);
            wybranePozycje = new ArrayList<String>();
            wybranePozycje.clear();
            if (!(wybranePozycje.contains(wybor)) && !(wybranePozycjePomoc.contains(wybor)) ) {
                wybranePozycjePomoc.add(wybor);
                wybranePozycje.add(wybor);
            }
                    while (wybranePozycje.size() <= 2) {
                        while (wybranePozycje.contains(wybor) && wybranePozycjePomoc.contains(wybor)) {
                            switch (przesuniecie) { //poczatek switcha
                                case 0: //poczatek case 0
                                    liczba2--;
                                    if (liczba2 < 0) { //poczatek if
                                        liczba2 = liczba2 + 2;
                                    } //koniec if
                                    wybor = Character.toString(pion[liczba1]) + Character.toString(poziom[liczba2]);
                                    break; //koniec case 0
                                case 1: //poczatek case 2
                                    liczba2++;
                                    if (liczba2 > 6) { //poczatek if
                                        liczba2 = liczba2 - 2;
                                    } //koniec if
                                    wybor = Character.toString(pion[liczba1]) + Character.toString(poziom[liczba2]);
                                    break; //koniec case 2
                                case 2: //poczatek case 1
                                    liczba1--;
                                    if (liczba1 < 0) { //poczatek if
                                        liczba1 = liczba1 + 2;
                                    } //koniec if
                                    wybor = Character.toString(pion[liczba1]) + Character.toString(poziom[liczba2]);
                                    break; //koniec case 1
                                case 3: //poczatek case 3
                                    liczba1++;
                                    if (liczba1 > 6) { //poczatek if
                                        liczba1 = liczba1 - 2;
                                    } //koniec if
                                    wybor = Character.toString(pion[liczba1]) + Character.toString(poziom[liczba2]);
                                    break; //koniec case 3
                            } //koniec switcha
                            if (przesuniecie == 0 || przesuniecie == 1) {
                                przesuniecie = (int) (Math.random() * 2);
                            } else przesuniecie = (int) ((Math.random() + 1) * 2);
                        }
                        if (!wybranePozycje.contains(wybor)) {
                            wybranePozycje.add(wybor);
                            wybranePozycjePomoc.add(wybor);
                        }
                    }
                    calosc.add(wybranePozycje);
        }
    }
    void wypisaniePozycji ()    {
        System.out.println("pozycja: " + wybranePozycje.get(0));
        System.out.println("pozycja: " + wybranePozycje.get(1));
        System.out.println("pozycja: " + wybranePozycje.get(2));
        System.out.println("calosc " + calosc.get(0));
        System.out.println("calosc " + calosc);
    }

}


